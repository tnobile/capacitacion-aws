const { DownstreamCommand } = require('ebased/schema/downstreamCommand');

class CreateClientCommand extends DownstreamCommand{
    constructor(payload, meta) {
        super({
            type: 'CREATE_CLIENT_COMMAND_CREATE',
            specversion: 'v1.0.0',
            payload: payload,
            meta: meta,
            schema: {
                strict: true,
                dni: { type: String, required: true},
                name: { type: String, required: true},
                lastName: {type: String, required: true},
                birthDate: { type: String, required: true}
            }

        })
    }
}

module.exports = { CreateClientCommand };