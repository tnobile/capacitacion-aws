const { DownStreamEvent } = require('ebased/schema/downstreamEvent');

class CreateClientEvent extends DownStreamEvent {
    constructor(payload, meta) {
        super({
          type: 'CLIENT.CREATED.CLIENT',
          specversion: 'v1.0.0',
          payload: payload,
          meta: meta,
          schema: {
              strict: true,
              dni: {type: String, required: true},
              name: {type: String, required: true},
              lastName: {type: String, required: true},
              birthDate: {type: String, required: true},
          },
        })
    }
}

module.exports = { CreateClientEvent };