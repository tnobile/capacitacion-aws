const dynamoDb = require('aws-sdk/clients/dynamodb');

const db = new dynamoDb();

const table = process.env.CLIENTS_TABLE;

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

exports.handler = async (event) => {
    
    const today = new Date();
    const dateOfBirthday = new Date(event.birthDate);
    const age = today.getFullYear() - dateOfBirthday.getFullYear();
    const months = today.getMonth() - dateOfBirthday.getMonth();
    
    if (months < 0 || (months === 0 && today.getDate() < dateOfBirthday.getDate())) {
        age--;
    }

    const queue = event.Records.map((record) => record.body);

    for await (const item of queue) {
        const msg = JSON.parse(item);
        const body = JSON.parse(msg.Message);

        const today = new Date();
        const dateOfBirthday = new Date(body.birthDate);
        const age = today.getFullYear() - dateOfBirthday.getFullYear();
        const months = today.getMonth() - dateOfBirthday.getMonth();
        
        if (months < 0 || (months === 0 && today.getDate() < dateOfBirthday.getDate())) {
            age--;
        }

        const cardCreditNumber = `${randomNumber(0123, 9876)}+${randomNumber(0123, 9876)}+${randomNumber(0123,9876)}+${randomNumber(0123, 9876)}`;
        const ccv = `${randomNumber(000, 999)}`;
        const expirate = `${randomNumber(01, 12)}/${randomNumber(22,27)}`;

        const type = age > 45 ? 'Gold' : 'Classic';

        const paramsOfDb = {
            ExpressionAttributeNames: {
                "#card": "creditCard",
            },
            ExpressionAttributeValues: {
                ":card": {
                    M: {
                        "number": {
                            S: cardCreditNumber,
                        },
                        "expirationDate": {
                            S: expirate,
                        },
                        "type": {
                            S: type,
                        },
                        "ccv": {
                            S: ccv,
                        },
                    }
                }
            },
            Key: {
                "dni": {
                    S: body.dni,
                },
            },
            TableName: table,
            UpdateExpression: "SET #card = :card"
        };

        try {
            await db.updateItem(paramsOfDb).promise();
        } catch (error) {
            return {
                statusCode: 500,
                body: error,
            };
        }

    }

    return {
        statusCode: 200,
        body: 'Success',
    };
};