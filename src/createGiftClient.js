const dynamoDb = require('aws-sdk/clients/dynamodb');

const db = new dynamoDb();

const table = process.env.CLIENTS_TABLE;

exports.handler = async (event) => {
    const queue = event.Records.map((record) => record.body);

    for await (const item of queue) {
        const msg = JSON.parse(item);
        const body = JSON.parse(msg.Message);

        const paramsOfDb = {
            ExpressionAttributeNames: {
                "#gift": "gift",
              },
              ExpressionAttributeValues: {
                ":gift": {
                  S: sendGift(body.birthdate),
                },
              },
              Key: {
                "dni": {
                  S: body.dni,
                },
              },
              TableName: table,
              UpdateExpression: "SET #gift = :gift"
        
        };

        try {
            await db.updateItem(paramsOfDb).promise();
            
        } catch (error) {
            return {
                statusCode: 500,
                body: error
            }
        }
    }

    return {
        statusCode: 200,
        body: 'Success',
    };
};


function summer(birth){
    const divideBirth = birth.split('/');
    const dayAndMonth = divideBirth[1] + '/' + divideBirth[2];
    return (dayAndMonth >= '12/21' && dayAndMonth <= '12/31') || (dayAndMonth >= '01/01' && dayAndMonth <= '03-20');
    }
    
    function winter(birth) {
        const divideBirth = birth.split('/');
        const dayAndMonth = divideBirth[1] + '/' + divideBirth[2];
        return dayAndMonth >= '06/21' && dayAndMonth <= '09/20';
    }
    
    function spring(birth) {
        const divideBirth = birth.split('/');
        const dayAndMonth = divideBirth[1] + '/' + divideBirth[2];
        return dayAndMonth >= '09/21' && dayAndMonth <= '12/20';
    }
    
    function fall(birth) {
        const divideBirth = birth.split('/');
        const dayAndMonth = divideBirth[1] + '/' + divideBirth[2];
        return dayAndMonth >= '03/21' && dayAndMonth <= '06/20';
    }
    
    function sendGift(birth) {
        if (fall(birth)) {
            return 'buzo'
        }
    
        if (spring(birth)) {
            return 'camisa'
        }
    
        if (winter(birth)) {
            return 'sweater'
        }
    
        if (summer(birth)) {
            return 'remera'
        }
    }