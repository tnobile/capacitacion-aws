const sns = require('ebased/service/downstream/sns');


const emitClientCreated = async (createClientEvent) => {
    const { eventPayload, eventMeta } = createClientEvent.get();
    const snsPublishParams = {
        TopicArn: process.env.SNS_TOPIC,
        Message: eventPayload,
    };
    return sns.publish(snsPublishParams, eventMeta);
}

module.exports = { emitClientCreated };