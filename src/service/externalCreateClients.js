const dynamo = require('ebased/service/storage/dynamo');

const externalCreateClient = async (createClientCommand) => {
    const { commandPayload, commandMeta } = createClientCommand.get();

    const paramsOfDb = {
        Item: commandPayload,
        TableName: process.env.CLIENTS_TABLE
    };

    return dynamo.putItem(paramsOfDb);

}

module.exports = { externalCreateClient };