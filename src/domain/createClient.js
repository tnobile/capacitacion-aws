const { CreateClientValidation } = require('../schema/input/createClientValidation');
const { CreateClientCommand } = require('../schema/command/createClientCommand');
const { CreateClientEvent } = require('../schema/event/createClient');
const { externalCreateClient } = require ('../service/externalCreateClients');
const { emitClientCreated } = require('../service/externalEmitClientCreated');

module.exports.createClient = async (commandPayload, commandMeta) => {
    new CreateClientValidation(commandPayload, commandMeta);
    await emitClientCreated(new CreateClientEvent(commandPayload, commandMeta));
    await externalCreateClient(new CreateClientCommand(commandPayload, commandMeta));

    return {
      body: 'Successful process'
    }
  
};

